package eu.dnetlib.authentication.services;

import eu.dnetlib.authentication.configuration.Properties;
import eu.dnetlib.authentication.entities.TokenResponse;
import eu.dnetlib.authentication.entities.User;
import eu.dnetlib.authentication.exception.ResourceNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mitre.oauth2.model.RegisteredClient;
import org.mitre.openid.connect.config.ServerConfiguration;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
public class UserInfoService {

    private static final Logger logger = LogManager.getLogger(UserInfoService.class);
    RestTemplate restTemplate;
    String issuer;
    RegisteredClient client;
    ServerConfiguration server;

    @Autowired
    public UserInfoService(RestTemplate restTemplate, Properties properties, RegisteredClient client, ServerConfiguration server) {
        this.restTemplate = restTemplate;
        this.issuer = properties.getOidc().getIssuer();
        this.server = server;
        this.client = client;
    }

    public User getUserInfo() throws ResourceNotFoundException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof OIDCAuthenticationToken) {
            return new User((OIDCAuthenticationToken) authentication);
        }
        throw new ResourceNotFoundException("No Session has been found");
    }

    public TokenResponse getAccessToken(String refreshToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(getTokenRequest(refreshToken), headers);
        return restTemplate.postForObject(this.server.getTokenEndpointUri(), entity, TokenResponse.class);
    }

    public MultiValueMap<String, String> getTokenRequest(String refreshToken) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", this.client.getClientId());
        map.add("client_secret", this.client.getClientSecret());
        map.add("grant_type", "refresh_token");
        map.add("refresh_token", refreshToken);
        map.add("scope", "openid");
        return map;
    }

    public void revoke() {
        OIDCAuthenticationToken authentication = (OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(revokeTokenRequest(authentication.getRefreshTokenValue()), headers);
        try {
            restTemplate.exchange(server.getRevocationEndpointUri(), HttpMethod.POST, entity, String.class);
        } catch (Exception e) {
            logger.error("Couldn't revoke refresh Tokens");
        }

    }

    public MultiValueMap<String, String> revokeTokenRequest(String refreshToken) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", this.client.getClientId());
        map.add("client_secret", this.client.getClientSecret());
        map.add("token", refreshToken);
        return map;
    }
}
