package eu.dnetlib.authentication.services;

import eu.dnetlib.authentication.utils.PropertyReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component(value = "SecurityService")
public class SecurityService {

    Set<String> scopes;

    @Autowired
    public SecurityService(PropertyReader reader) {
        this.scopes = reader.getScopes();
    }

    public boolean hasRefreshToken() {
        return this.scopes.contains("offline_access");
    }
}
