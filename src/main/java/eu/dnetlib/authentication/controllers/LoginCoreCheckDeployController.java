package eu.dnetlib.authentication.controllers;

import eu.dnetlib.authentication.configuration.AuthenticationConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/login-core")
public class LoginCoreCheckDeployController {
    private final Logger log = LogManager.getLogger(this.getClass());
    private final AuthenticationConfiguration configuration;

    @Autowired
    public LoginCoreCheckDeployController(AuthenticationConfiguration configuration) {
        this.configuration = configuration;
    }

    @RequestMapping(value = {"", "/health_check"}, method = RequestMethod.GET)
    public String hello() {
        log.debug("Hello from Login Core");
        return "Hello from Login Core!";
    }

    @PreAuthorize("hasAnyAuthority('PORTAL_ADMINISTRATOR')")
    @RequestMapping(value = "/health_check/advanced", method = RequestMethod.GET)
    public Map<String, String> checkEverything() {
        Map<String, String> response = configuration.getProperties();
        return response;
    }
}
