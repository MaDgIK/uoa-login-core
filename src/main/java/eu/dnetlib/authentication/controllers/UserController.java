package eu.dnetlib.authentication.controllers;

import eu.dnetlib.authentication.entities.TokenResponse;
import eu.dnetlib.authentication.entities.User;
import eu.dnetlib.authentication.configuration.Properties;
import eu.dnetlib.authentication.services.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RestController
public class UserController {
    
    private final UserInfoService userInfoService;
    private final Properties properties;

    @Autowired
    public UserController(UserInfoService userInfoService, Properties properties) {
        this.userInfoService = userInfoService;
        this.properties = properties;
    }

    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public ResponseEntity<User> getUserInfo() {
        return ResponseEntity.ok(userInfoService.getUserInfo());
    }

    @RequestMapping(value = "/accessToken", method = RequestMethod.GET)
    @PreAuthorize("@SecurityService.hasRefreshToken()")
    public ResponseEntity<TokenResponse> getAccessToken(@RequestParam(name = "refreshToken") String refreshToken) {
        return ResponseEntity.ok(this.userInfoService.getAccessToken(refreshToken));
    }

    @RequestMapping(value = "/revoke", method = RequestMethod.POST)
    @PreAuthorize("@SecurityService.hasRefreshToken()")
    public void revoke() {
        this.userInfoService.revoke();
    }

    @RequestMapping(value = "/redirect", method = RequestMethod.GET)
    public void redirect(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        String redirect = (String) session.getAttribute("redirect");
        session.removeAttribute("redirect");
        if(redirect == null) {
            redirect = properties.getRedirect();
        }
        session.invalidate();
        response.sendRedirect(redirect);
    }
}
