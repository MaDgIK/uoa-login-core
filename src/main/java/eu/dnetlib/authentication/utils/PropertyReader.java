package eu.dnetlib.authentication.utils;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class PropertyReader {

    Set<String> scopes;

    public PropertyReader(String property) {
        if (!property.trim().isEmpty()){
            scopes = new HashSet<>();
            Collections.addAll(scopes, property.split(","));
        }
    }

    public Set<String> getScopes() {
        return scopes;
    }

    public void setScopes(Set<String> scopes) {
        this.scopes = scopes;
    }
}
