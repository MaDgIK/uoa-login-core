package eu.dnetlib.authentication.utils;


import eu.dnetlib.authentication.configuration.Properties;
import org.apache.http.client.utils.URIBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.net.URISyntaxException;
import java.util.Enumeration;

public class Redirect {

    private final static Logger logger = LogManager.getLogger(Redirect.class);

    private static String getDomain(String url) throws URISyntaxException {
        URIBuilder uriBuilder = new URIBuilder(url);
        return uriBuilder.getHost();
    }

    public static void setRedirect(HttpServletRequest request, Properties properties) {
        HttpSession session = request.getSession();
        Enumeration<String> params = request.getParameterNames();
        while (params.hasMoreElements()) {
            String param = params.nextElement();
            if(param.equalsIgnoreCase("redirect")) {
                String redirect = request.getParameter(param);
                try {
                    if(getDomain(redirect).endsWith(properties.getDomain())) {
                        session.setAttribute("redirect", redirect);
                    }
                } catch (URISyntaxException e) {
                    logger.error(e.getMessage());
                }
            }
        }
    }
}
