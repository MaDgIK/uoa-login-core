package eu.dnetlib.authentication.security.oidc;

import eu.dnetlib.authentication.configuration.Properties;
import eu.dnetlib.authentication.utils.Redirect;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mitre.openid.connect.client.OIDCAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class OpenAIREAuthenticationFilter extends OIDCAuthenticationFilter {

    private final static Logger logger = LogManager.getLogger(OpenAIREAuthenticationSuccessHandler.class);
    private final Properties properties;

    public OpenAIREAuthenticationFilter(Properties properties) {
        super();
        this.properties = properties;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequestWrapper wrapped = new HttpServletRequestWrapper((HttpServletRequest) req) {
            @Override
            public StringBuffer getRequestURL() {
                final StringBuffer originalUrl = ((HttpServletRequest) getRequest()).getRequestURL();
                if(originalUrl.toString().contains(OIDCAuthenticationFilter.FILTER_PROCESSES_URL)) {
                    return new StringBuffer(properties.getOidc().getHome());
                } else if(properties.getOidc().getRedirect() != null){
                    return new StringBuffer(properties.getOidc().getRedirect());
                } else {
                    return originalUrl;
                }
            }
        };
        super.doFilter(wrapped, res, chain);
    }

    @Override
    protected void handleAuthorizationRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Redirect.setRedirect(request, properties);
        super.handleAuthorizationRequest(request, response);
    }
}
