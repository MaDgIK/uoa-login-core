package eu.dnetlib.authentication.security.oidc;

import com.nimbusds.jwt.JWT;
import org.mitre.openid.connect.client.OIDCAuthoritiesMapper;
import org.mitre.openid.connect.model.UserInfo;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.HashSet;

public class DefaultAuthoritiesMapper implements OIDCAuthoritiesMapper {

    @Override
    public Collection<? extends GrantedAuthority> mapAuthorities(JWT jwtToken, UserInfo userInfo) {
        return new HashSet<>();
    }
}
