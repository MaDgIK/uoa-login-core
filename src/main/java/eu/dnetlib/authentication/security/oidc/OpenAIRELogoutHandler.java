package eu.dnetlib.authentication.security.oidc;

import eu.dnetlib.authentication.configuration.Properties;
import eu.dnetlib.authentication.utils.Redirect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class OpenAIRELogoutHandler implements LogoutHandler {

    private final Properties properties;

    @Autowired
    public OpenAIRELogoutHandler(Properties properties) {
        this.properties = properties;
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        Redirect.setRedirect(request, properties);
    }
}
