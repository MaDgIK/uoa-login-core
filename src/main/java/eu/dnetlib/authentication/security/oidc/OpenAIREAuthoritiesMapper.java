package eu.dnetlib.authentication.security.oidc;

import com.google.gson.JsonArray;
import com.nimbusds.jwt.JWT;
import eu.dnetlib.authentication.configuration.Properties;
import eu.dnetlib.authentication.utils.AuthoritiesMapper;
import org.mitre.openid.connect.client.OIDCAuthoritiesMapper;
import org.mitre.openid.connect.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
@ConditionalOnProperty(
        value="authentication.authorities-mapper",
        havingValue = "eduperson_entitlement")
public class OpenAIREAuthoritiesMapper implements OIDCAuthoritiesMapper {

    private final Properties properties;

    @Autowired
    OpenAIREAuthoritiesMapper(Properties properties) {
        this.properties = properties;
    }

    @Override
    public Collection<? extends GrantedAuthority> mapAuthorities(JWT jwtToken, UserInfo userInfo) {
        JsonArray entitlements = userInfo.getSource().getAsJsonArray(properties.getAuthoritiesMapper());
        return AuthoritiesMapper.map(entitlements);
    }
}
