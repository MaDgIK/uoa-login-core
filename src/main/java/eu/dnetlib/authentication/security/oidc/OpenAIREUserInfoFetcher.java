package eu.dnetlib.authentication.security.oidc;

import org.mitre.openid.connect.client.UserInfoFetcher;
import org.mitre.openid.connect.model.PendingOIDCAuthenticationToken;
import org.mitre.openid.connect.model.UserInfo;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Component
public class OpenAIREUserInfoFetcher extends UserInfoFetcher {

    public OpenAIREUserInfoFetcher() {}

    @Override
    public UserInfo loadUserInfo(PendingOIDCAuthenticationToken token) {
        UserInfo userInfo = super.loadUserInfo(token);
        userInfo.setGivenName(encoder(userInfo.getGivenName()));
        userInfo.setFamilyName(encoder(userInfo.getFamilyName()));
        userInfo.setName(encoder(userInfo.getName()));
        return userInfo;
    }

    private String encoder(String value) {
        String decodedString = new String(value.getBytes(StandardCharsets.UTF_8));
        return new String(decodedString.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
    }
}
