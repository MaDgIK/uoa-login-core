package eu.dnetlib.authentication.security.oidc;

import eu.dnetlib.authentication.configuration.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Configuration
public class OpenAIRELogoutSuccessHandler implements LogoutSuccessHandler {

    private final Properties properties;

    @Autowired
    public OpenAIRELogoutSuccessHandler(Properties properties) {
        this.properties = properties;
    }

    private String encodeValue(String value) throws UnsupportedEncodingException {
        return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        if(properties.getOidc().getRedirect() == null) {
            HttpSession session = request.getSession();
            String redirect = (String) session.getAttribute("redirect");
            session.removeAttribute("redirect");
            if(redirect == null) {
                redirect = properties.getRedirect();
            }
            session.invalidate();
            response.sendRedirect(properties.getOidc().getLogout() + encodeValue(redirect));
        } else {
            StringBuilder sb = new StringBuilder(properties.getOidc().getIssuer());
            if(properties.getKeycloak()) {
                sb.append("/protocol/openid-connect/logout");
                sb.append("?client_id=").append(properties.getOidc().getId());
                sb.append("&post_logout_redirect_uri=").append(encodeValue(properties.getOidc().getRedirect()));
            } else {
                sb.append("saml/logout");
            }
            response.sendRedirect(sb.toString());
        }
    }
}
