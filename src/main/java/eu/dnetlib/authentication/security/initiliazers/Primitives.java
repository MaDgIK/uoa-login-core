package eu.dnetlib.authentication.security.initiliazers;

import eu.dnetlib.authentication.configuration.Properties;
import eu.dnetlib.authentication.utils.EntryPoint;
import eu.dnetlib.authentication.utils.PropertyReader;
import org.mitre.openid.connect.client.service.impl.PlainAuthRequestUrlBuilder;
import org.mitre.openid.connect.client.service.impl.StaticAuthRequestOptionsService;
import org.mitre.openid.connect.client.service.impl.StaticSingleIssuerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;

@Configuration
public class Primitives {

    private final Properties properties;

    @Autowired
    public Primitives(Properties properties) {
        this.properties = properties;

    }

    @Bean
    public PropertyReader scopeReader() {
        return new PropertyReader(this.properties.getOidc().getScope());
    }

    @Bean
    public DefaultWebSecurityExpressionHandler handler() {
        return new DefaultWebSecurityExpressionHandler();
    }

    @Bean
    public PlainAuthRequestUrlBuilder builder() {
        return new PlainAuthRequestUrlBuilder();
    }

    @Bean
    public StaticSingleIssuerService issuerService() {
        StaticSingleIssuerService issuerService = new StaticSingleIssuerService();
        issuerService.setIssuer(properties.getOidc().getIssuer());
        return issuerService;
    }

    @Bean
    public EntryPoint entryPoint() {
        return new EntryPoint();
    }

    @Bean
    public StaticAuthRequestOptionsService optionsService() {
        return new StaticAuthRequestOptionsService();
    }
}
