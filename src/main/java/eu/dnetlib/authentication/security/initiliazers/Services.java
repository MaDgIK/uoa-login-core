package eu.dnetlib.authentication.security.initiliazers;

import eu.dnetlib.authentication.configuration.Properties;
import org.mitre.oauth2.model.RegisteredClient;
import org.mitre.openid.connect.client.service.impl.StaticClientConfigurationService;
import org.mitre.openid.connect.client.service.impl.StaticServerConfigurationService;
import org.mitre.openid.connect.config.ServerConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class Services {

    private final Properties properties;
    private final ServerConfiguration serverConfiguration;
    private final RegisteredClient clientConfiguration;

    @Autowired
    public Services(Properties properties, ServerConfiguration serverConfiguration, RegisteredClient clientConfiguration) {
        this.properties = properties;
        this.serverConfiguration = serverConfiguration;
        this.clientConfiguration = clientConfiguration;
    }


    @Bean
    public StaticServerConfigurationService serverConfigurationService() {
        StaticServerConfigurationService configurationService = new StaticServerConfigurationService();
        Map<String, ServerConfiguration> servers = new HashMap<>();
        servers.put(properties.getOidc().getIssuer(), serverConfiguration);
        configurationService.setServers(servers);
        return configurationService;
    }

    @Bean
    public StaticClientConfigurationService clientConfigurationService() {
        StaticClientConfigurationService configurationService = new StaticClientConfigurationService();
        Map<String, RegisteredClient> clients = new HashMap<>();
        clients.put(properties.getOidc().getIssuer(), clientConfiguration);
        configurationService.setClients(clients);
        return configurationService;
    }
}
