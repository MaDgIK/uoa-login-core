package eu.dnetlib.authentication.security.initiliazers;

import eu.dnetlib.authentication.configuration.Properties;
import eu.dnetlib.authentication.security.oidc.DefaultAuthoritiesMapper;
import eu.dnetlib.authentication.security.oidc.OpenAIREUserInfoFetcher;
import eu.dnetlib.authentication.utils.PropertyReader;
import org.mitre.oauth2.model.ClientDetailsEntity;
import org.mitre.oauth2.model.RegisteredClient;
import org.mitre.openid.connect.client.OIDCAuthenticationProvider;
import org.mitre.openid.connect.client.OIDCAuthoritiesMapper;
import org.mitre.openid.connect.config.ServerConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

@Configuration
public class Configurations {

    private final Properties properties;
    private final PropertyReader scopeReader;
    private final OpenAIREUserInfoFetcher userInfoFetcher;
    private final OIDCAuthoritiesMapper authoritiesMapper;

    @Autowired
    public Configurations(Properties properties, Optional<OIDCAuthoritiesMapper> authoritiesMapper, OpenAIREUserInfoFetcher userInfoFetcher, PropertyReader scopeReader) {
        this.properties = properties;
        this.authoritiesMapper = authoritiesMapper.orElse(new DefaultAuthoritiesMapper());
        this.userInfoFetcher = userInfoFetcher;
        this.scopeReader = scopeReader;
    }

    @Bean
    public OIDCAuthenticationProvider provider() {
        OIDCAuthenticationProvider provider = new OIDCAuthenticationProvider();
        if(properties.getKeycloak()) {
            provider.setUserInfoFetcher(this.userInfoFetcher);
        }
        if(this.authoritiesMapper != null) {
            provider.setAuthoritiesMapper(this.authoritiesMapper);
        }
        return provider;
    }

    @Bean
    public ServerConfiguration serverConfiguration() {
        String issuer = properties.getOidc().getIssuer();
        ServerConfiguration serverConfiguration = new ServerConfiguration();
        serverConfiguration.setIssuer(issuer);
        Boolean keycloak = properties.getKeycloak();
        if(keycloak) {
            serverConfiguration.setAuthorizationEndpointUri(issuer + "/protocol/openid-connect/auth");
            serverConfiguration.setTokenEndpointUri(issuer + "/protocol/openid-connect/token");
            serverConfiguration.setUserInfoUri(issuer + "/protocol/openid-connect/userinfo");
            serverConfiguration.setJwksUri(issuer + "/protocol/openid-connect/certs");
            serverConfiguration.setRevocationEndpointUri(issuer + "/protocol/openid-connect/revoke");
        } else {
            serverConfiguration.setAuthorizationEndpointUri(issuer + "authorize");
            serverConfiguration.setTokenEndpointUri(issuer + "token");
            serverConfiguration.setUserInfoUri(issuer + "userinfo");
            serverConfiguration.setJwksUri(issuer + "jwk");
            serverConfiguration.setRevocationEndpointUri(issuer + "revoke");
        }
        return serverConfiguration;
    }

    @Bean
    public RegisteredClient registeredClient() {
        RegisteredClient client = new RegisteredClient();
        client.setClientId(properties.getOidc().getId());
        client.setClientSecret(properties.getOidc().getSecret());
        client.setScope(scopeReader.getScopes());
        client.setTokenEndpointAuthMethod(ClientDetailsEntity.AuthMethod.SECRET_BASIC);
        client.setRedirectUris(new HashSet<>(Arrays.asList(properties.getOidc().getHome(), properties.getOidc().getRedirect())));
        return client;
    }
}
