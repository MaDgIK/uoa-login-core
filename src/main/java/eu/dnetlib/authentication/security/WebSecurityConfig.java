package eu.dnetlib.authentication.security;

import eu.dnetlib.authentication.configuration.Properties;
import eu.dnetlib.authentication.security.oidc.OpenAIREAuthenticationFilter;
import eu.dnetlib.authentication.security.oidc.OpenAIREAuthenticationSuccessHandler;
import eu.dnetlib.authentication.security.oidc.OpenAIRELogoutHandler;
import eu.dnetlib.authentication.security.oidc.OpenAIRELogoutSuccessHandler;
import eu.dnetlib.authentication.utils.EntryPoint;
import org.mitre.openid.connect.client.OIDCAuthenticationProvider;
import org.mitre.openid.connect.client.service.ClientConfigurationService;
import org.mitre.openid.connect.client.service.IssuerService;
import org.mitre.openid.connect.client.service.ServerConfigurationService;
import org.mitre.openid.connect.client.service.impl.PlainAuthRequestUrlBuilder;
import org.mitre.openid.connect.client.service.impl.StaticAuthRequestOptionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, proxyTargetClass = true)
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final Properties properties;
    private final EntryPoint entryPoint;
    private final OIDCAuthenticationProvider provider;
    private final IssuerService issuerService;
    private final ServerConfigurationService serverConfigurationService;
    private final ClientConfigurationService clientConfigurationService;
    private final StaticAuthRequestOptionsService optionsService;
    private final PlainAuthRequestUrlBuilder builder;
    private final OpenAIREAuthenticationSuccessHandler authenticationSuccessHandler;
    private final OpenAIRELogoutHandler logoutHandler;
    private final OpenAIRELogoutSuccessHandler logoutSuccessHandler;

    @Autowired
    public WebSecurityConfig(Properties properties, EntryPoint entryPoint, OIDCAuthenticationProvider provider,
                             IssuerService issuerService, ServerConfigurationService serverConfigurationService,
                             ClientConfigurationService clientConfigurationService, StaticAuthRequestOptionsService optionsService,
                             PlainAuthRequestUrlBuilder builder, OpenAIREAuthenticationSuccessHandler authenticationSuccessHandler,
                             OpenAIRELogoutHandler logoutHandler, OpenAIRELogoutSuccessHandler logoutSuccessHandler) {
        super();
        this.properties = properties;
        this.entryPoint = entryPoint;
        this.provider = provider;
        this.issuerService = issuerService;
        this.serverConfigurationService = serverConfigurationService;
        this.clientConfigurationService = clientConfigurationService;
        this.optionsService = optionsService;
        this.builder = builder;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
        this.logoutHandler = logoutHandler;
        this.logoutSuccessHandler = logoutSuccessHandler;
    }

    public OpenAIREAuthenticationFilter initFilter() throws Exception {
        OpenAIREAuthenticationFilter filter = new OpenAIREAuthenticationFilter(properties);
        filter.setAuthenticationManager(authenticationManagerBean());
        filter.afterPropertiesSet();
        filter.setIssuerService(issuerService);
        filter.setServerConfigurationService(serverConfigurationService);
        filter.setClientConfigurationService(clientConfigurationService);
        filter.setAuthRequestOptionsService(optionsService);
        filter.setAuthRequestUrlBuilder(builder);
        filter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
        return filter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authenticationProvider(provider);
        http.addFilterBefore(initFilter(), BasicAuthenticationFilter.class);
        http.httpBasic().authenticationEntryPoint(entryPoint);
        http.logout().logoutUrl("/openid_logout").addLogoutHandler(logoutHandler)
                .logoutSuccessHandler(logoutSuccessHandler).invalidateHttpSession(false);
        http.authorizeRequests().anyRequest().permitAll();
    }

}
