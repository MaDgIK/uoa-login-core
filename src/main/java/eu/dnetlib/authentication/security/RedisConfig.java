package eu.dnetlib.authentication.security;

import eu.dnetlib.authentication.configuration.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

@EnableRedisHttpSession
@Configuration
public class RedisConfig {

    private final Properties properties;

    private static final Logger logger = LogManager.getLogger(RedisConfig.class);

    @Autowired
    public RedisConfig(Properties properties) {
        this.properties = properties;
    }

    @Bean
    public LettuceConnectionFactory connectionFactory() {
        logger.info(String.format("Redis connection listens to %s:%s ", properties.getRedis().getHost(), properties.getRedis().getPort()));
        LettuceConnectionFactory factory = new LettuceConnectionFactory(properties.getRedis().getHost(), Integer.parseInt(properties.getRedis().getPort()));
        if (properties.getRedis().getPassword() != null) factory.setPassword(properties.getRedis().getPassword());
        return factory;
    }

    @Bean
    public CookieSerializer cookieSerializer() {
        DefaultCookieSerializer serializer = new DefaultCookieSerializer();
        serializer.setCookieName(properties.getSession());
        serializer.setCookiePath("/");
        serializer.setDomainName(properties.getDomain());
        return serializer;
    }
}
