package eu.dnetlib.authentication.entities;

public class TokenResponse {
    String access_token;
    Long expires_in;
    String id_token;
    String refresh_token;
    String scope;
    String token_type;

    public TokenResponse() {
    }

    public String getAccess_token() {
        return access_token;
    }

    public Long getExpires_in() {
        return expires_in;
    }

    public String getId_token() {
        return id_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public String getScope() {
        return scope;
    }

    public String getToken_type() {
        return token_type;
    }
}
