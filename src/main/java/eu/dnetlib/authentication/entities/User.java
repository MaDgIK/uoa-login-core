package eu.dnetlib.authentication.entities;

import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

public class User {

    private String sub;
    private String name;
    private String given_name;
    private String family_name;
    private String email;
    private String orcid;
    private Set<String> roles;
    private String accessToken;
    private String refreshToken;

    public User(OIDCAuthenticationToken token) {
        this.sub = token.getUserInfo().getSub();
        this.name = token.getUserInfo().getName();
        this.given_name = token.getUserInfo().getGivenName();
        this.family_name = token.getUserInfo().getFamilyName();
        this.email = token.getUserInfo().getEmail();
        if(token.getUserInfo().getSource().get("orcid") != null) {
            this.orcid = token.getUserInfo().getSource().get("orcid").getAsString();
        }
        this.roles = token.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
        this.accessToken = token.getAccessTokenValue();
        this.refreshToken = token.getRefreshTokenValue();
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGiven_name() {
        return given_name;
    }

    public void setGiven_name(String given_name) {
        this.given_name = given_name;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrcid() {
        return orcid;
    }

    public void setOrcid(String orcid) {
        this.orcid = orcid;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
