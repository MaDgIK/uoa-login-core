package eu.dnetlib.authentication.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("authentication")
public class Properties {

    private Redis redis = new Redis();
    private OIDC oidc = new OIDC();
    private String domain;
    private String session;
    private String accessToken;
    private String redirect;
    private String authoritiesMapper;
    private Boolean keycloak;

    public Properties() {
    }

    public Redis getRedis() {
        return redis;
    }

    public void setRedis(Redis redis) {
        this.redis = redis;
    }

    public OIDC getOidc() {
        return oidc;
    }

    public void setOidc(OIDC oidc) {
        this.oidc = oidc;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getAuthoritiesMapper() {
        return authoritiesMapper;
    }

    public void setAuthoritiesMapper(String authoritiesMapper) {
        this.authoritiesMapper = authoritiesMapper;
    }

    public Boolean getKeycloak() {
        return keycloak;
    }

    public void setKeycloak(Boolean keycloak) {
        this.keycloak = keycloak;
    }
}
