package eu.dnetlib.authentication.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableConfigurationProperties({Properties.class, GlobalVars.class})
@ComponentScan(basePackages = {"eu.dnetlib.authentication"})
public class AuthenticationConfiguration {

    private final Properties properties;
    private final GlobalVars globalVars;

    @Autowired
    public AuthenticationConfiguration(Properties properties, GlobalVars globalVars) {
        this.properties = properties;
        this.globalVars = globalVars;
    }

    public Map<String, String> getProperties() {
        Map<String, String> map = new HashMap<>();
        map.put("authentication.domain", properties.getDomain());
        map.put("authentication.keycloak", properties.getKeycloak().toString());
        map.put("authentication.redis.host", properties.getRedis().getHost());
        map.put("authentication.oidc.issuer", properties.getOidc().getIssuer());
        map.put("authentication.oidc.logout", properties.getOidc().getLogout());
        map.put("authentication.oidc.home", properties.getOidc().getHome());
        map.put("authentication.oidc.redirect", properties.getOidc().getRedirect());
        map.put("authentication.oidc.scope", properties.getOidc().getScope());
        map.put("authentication.oidc.id", properties.getOidc().getId());
        map.put("authentication.oidc.secret", properties.getOidc().getSecret());
        map.put("authentication.session", properties.getSession());
        map.put("authentication.accessToken", properties.getAccessToken());
        map.put("authentication.redirect", properties.getRedirect());
        map.put("authentication.authorities-mapper", properties.getAuthoritiesMapper());
        if(GlobalVars.date != null) {
            map.put("Date of deploy", GlobalVars.date.toString());
        }
        if(globalVars.getBuildDate() != null) {
            map.put("Date of build", globalVars.getBuildDate());
        }
        if (globalVars.getVersion() != null) {
            map.put("Version", globalVars.getVersion());
        }
        return map;
    }

    @Bean
    RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(new ObjectMapper());
        restTemplate.getMessageConverters().add(converter);
        return restTemplate;
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods("GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS")
                        .allowCredentials(true);
            }
        };
    }
}
